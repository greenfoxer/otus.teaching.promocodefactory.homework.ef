using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public static class PreferenceMapper
    {
        public static PreferenceShortResponse MapModelToShortResponse(Preference pref)
        {
            return new PreferenceShortResponse(){
                Id = pref.Id.ToString(),
                Name = pref.Name
            };
        }
    }
}