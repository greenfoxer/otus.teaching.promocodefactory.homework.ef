using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public static class CustomerMapper
    {
        public static CustomerResponse MapModelToResponse(Customer customer)
        {
            var DataPromoCodes = customer.Promocodes == null ? new List<PromoCodeShortResponse>() : customer.Promocodes.Select(t => PromocodeMapper.MapModelToShortResponse(t)).ToList();
            var DataPreferences = customer.CustomerPreferences ==null ? 
                new List<PreferenceShortResponse>() :
                customer.CustomerPreferences.Select(c => new PreferenceShortResponse() { Id = c.Preference.Id.ToString(), Name = c.Preference.Name}).ToList();
            
            return new CustomerResponse(){
                Id= customer.Id,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    Email = customer.Email,
                    Preferences = DataPreferences,
                    PromoCodes = DataPromoCodes
            };            
        }
        public static CustomerShortResponse MapModelToShortResponse(Customer customer)
        {
            return new CustomerShortResponse(){
                Id= customer.Id,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    Email = customer.Email
            };
        }
        public static Customer MapCERequestToModel(CreateOrEditCustomerRequest cer, string id = "00000000-0000-0000-0000-000000000000")
        {
            var newGuid = Guid.Parse(id) == Guid.Empty ? Guid.NewGuid() : Guid.Parse(id);
            return new Customer(){
                Id = newGuid,
                FirstName = cer.FirstName,
                LastName = cer.LastName,
                Email = cer.Email,
                CustomerPreferences = cer.PreferenceIds.Select(t => new CustomerPreference() { CustomerID = newGuid, PreferenceID = t}).ToList()
            };
        }
    }
}