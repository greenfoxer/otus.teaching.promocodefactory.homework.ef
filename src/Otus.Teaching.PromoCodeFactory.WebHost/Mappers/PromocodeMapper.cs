using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public static class PromocodeMapper
    {
        public static PromoCodeShortResponse MapModelToShortResponse(PromoCode promo)
        {
            return new PromoCodeShortResponse() { 
                        Id = promo.Id,
                        Code = promo.Code,
                        ServiceInfo = promo.ServiceInfo,
                        BeginDate = promo.BeginDate.ToString(),
                        EndDate = promo.EndDate.ToString(),
                        PartnerName = promo.PartnerName
                        };
        }
        public static PromoCode MapCreateSimpleRequestToModel(CreateSimplePromocodeRequest request)
        {
            return new PromoCode(){
                Id = Guid.NewGuid(),
                Code = request.Code,
                ServiceInfo = request.ServiceInfo,
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddMonths(12),
                PartnerName = request.PartnerName,
                PreferenceId = Guid.Parse(request.PreferenceId),
                PartnerManagerId = Guid.Parse(request.PartnerManagerId)
            };
        }
    }
}