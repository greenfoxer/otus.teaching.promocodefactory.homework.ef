using System.Text.RegularExpressions;
using System.Runtime.CompilerServices;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController
        : ControllerBase
    {
        IRepository<Preference> repository;
        public PreferenceController(IRepository<Preference> repo)
        {
            repository = repo;
        }
        
        /// <summary>
        /// Метод возвращает список всех имеющихся предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<PromoCodeShortResponse>> GetPreferencesAsync()
        {
            var result = (await repository.GetAllAsync()).Select(p => PreferenceMapper.MapModelToShortResponse(p));
            return Ok(result);
        }
    }
}