﻿using System.Runtime.CompilerServices;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        ICustomerRepository repository;
        public CustomersController(ICustomerRepository repo)
        {
            repository = repo;
        }
        /// <summary>
        /// Метод возвращает список клиентов в виде CustomerShortResponse
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            //TODO: Добавить получение списка клиентов
            var result = await repository.GetAllAsync();
            return Ok(result
            .Select( t => CustomerMapper.MapModelToShortResponse(t)) 
            .ToList());       
        }
        /// <summary>
        /// Возвращает детальную информацию о конкретном клиенте, вместе со списком промокодов и предпочтений
        /// </summary>
        /// <param name="id">Guid пользователя</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            //TODO: Добавить получение клиента вместе с выданными ему промомкодами
            var data = await repository.GetCustomerFullDataAsync(id);
            if(data != null)
            {
            var result = CustomerMapper.MapModelToResponse(data);
            return Ok(result);}
            else
                return StatusCode(404,$"Cant find user {id}");
        }
        /// <summary>
        /// Метод создания нового пользователя. 
        /// </summary>
        /// <param name="request">CreateOrEditCustomerRequest объект с информацией о новом пользователе.</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //TODO: Добавить создание нового клиента вместе с его предпочтениями
            try
            {
                var dataToAdd = CustomerMapper.MapCERequestToModel(request);
                await repository.AddAsync(dataToAdd);
                return Ok();
            }
            catch(Exception e)
            {
                return StatusCode(500, e.Message);
            }

        }
        /// <summary>
        /// Метод, осуществляющий редактирование информации о пользователе и его проедпочтениях
        /// </summary>
        /// <param name="id">Guid редактируемого объекта</param>
        /// <param name="request">CreateOrEditCustomerRequest объект с отредактированной информацией</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            //TODO: Обновить данные клиента вместе с его предпочтениями
            try
            {
                var item = CustomerMapper.MapCERequestToModel(request, id.ToString());
                await repository.UpdateCustomerAsync(item, request.PreferenceIds);
                return Ok();
            }
            catch (System.Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
        /// <summary>
        /// Метод осузествляет удаление пользователя вместе с его промокодами.
        /// </summary>
        /// <param name="id">Guid удаляемого объекта</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            //TODO: Удаление клиента вместе с выданными ему промокодами
            try
            {
                await repository.RemoveAsync(id);
                return Ok();
            }
            catch (System.Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
    }
}