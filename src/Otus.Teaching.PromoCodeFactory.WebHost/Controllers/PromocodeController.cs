using System.Text.RegularExpressions;
using System.Runtime.CompilerServices;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodeController
        : ControllerBase
    {
        IPromoCodeRepository repository;
        public PromocodeController(IPromoCodeRepository repo)
        {
            repository = repo;
        }
        
        /// <summary>
        /// Метод возвращает список всех имеющихся промокодов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<PromoCodeShortResponse>> GetPromocodesAsync()
        {
            var result = (await repository.GetAllAsync()).Select(p => PromocodeMapper.MapModelToShortResponse(p));
            return Ok(result);
        }
        /// <summary>
        /// Метод добавляет новый промокод все пользователем с совпадающим Preference
        /// </summary>
        /// <param name="request">CreateSimplePromocodeRequest - объект, содержащий информацию о новом промокоде</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> GivePromocodesToCustomersWithPreferenceAsync(CreateSimplePromocodeRequest request)
        {
            try
            {
                await repository.AddPromocodeByPreferenceAsync(PromocodeMapper.MapCreateSimpleRequestToModel(request));
                return Ok();
            }
            catch (System.Exception e)
            {
                return StatusCode(500, e);
            }
        }
    }
}