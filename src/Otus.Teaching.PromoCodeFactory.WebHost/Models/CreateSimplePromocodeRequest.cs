namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CreateSimplePromocodeRequest
    {
        public string Code { get; set; }
        public string ServiceInfo { get; set; }
        public string PartnerName { get; set; }
        public string PreferenceId {get; set;}
        public string PartnerManagerId {get;set;}
    }
}