namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferenceShortResponse
    {
        public string Id {get;set;}
        public string Name {get;set;}
    }
}