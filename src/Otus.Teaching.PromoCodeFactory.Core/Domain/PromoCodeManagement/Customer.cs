﻿using System.Linq;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        //TODO: Списки Preferences и Promocodes 
        public IEnumerable<CustomerPreference> CustomerPreferences {get;set;}
        public IEnumerable<PromoCode> Promocodes {get;set;}
        //New field for Task-8
        public int PromocodeCount {get; set;}

        public void AddNewPromoCode(PromoCode promo)
        {
            var id = Guid.NewGuid();
            promo.Id = id;
            promo.CustomerId = Id;
            var list = Promocodes.ToList();   
            list.Add(promo);
            Promocodes = list;
        }
    }
}