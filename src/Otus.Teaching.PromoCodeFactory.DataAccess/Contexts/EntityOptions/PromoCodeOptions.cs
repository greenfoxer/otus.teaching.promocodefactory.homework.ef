using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Contexts
{
    public class PromoCodeOptions : IEntityTypeConfiguration<PromoCode>
    {
        public void Configure(EntityTypeBuilder<PromoCode> builder)
        {
            builder.HasKey(p => p.Id);

            builder.Property(p => p.Code).HasMaxLength(100);
            builder.Property(p => p.ServiceInfo).HasMaxLength(150);
            builder.Property(p => p.PartnerName).HasMaxLength(100);
            
            builder.HasOne(p => p.Preference)
                .WithMany(r => r.Promocodes);
            builder.HasOne(p => p.Customer)
                .WithMany(c => c.Promocodes)
                .OnDelete(DeleteBehavior.Cascade);
            builder.HasOne(p => p.PartnerManager)
                .WithMany(e => e.GivenPromocodes)
                .HasForeignKey( p => p.PartnerManagerId);             
        }
    }
}