using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Contexts
{
    public class CustomerPreferenceOptions : IEntityTypeConfiguration<CustomerPreference>
    {
        public void Configure(EntityTypeBuilder<CustomerPreference> builder)
        {
            builder.HasKey(p => new {p.CustomerID, p.PreferenceID});
            
            builder.HasOne(p => p.Preference)
                .WithMany(t => t.CustomerPreferences)
                .HasForeignKey(s => s.PreferenceID);

            builder.HasOne(c => c.Customer)
                .WithMany( t => t.CustomerPreferences)
                .HasForeignKey( s => s.CustomerID)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}