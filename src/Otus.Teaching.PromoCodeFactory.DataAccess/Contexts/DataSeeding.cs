using System.Net.Mime;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Contexts
{
    public static class DataSeedingExtension
    {
        public static void Seed(this EFContext context)
        {
            if (context != null)
            {
                if(!context.Preferences.Any())
                {
                    context.Preferences.AddRange(FakeDataFactory.Preferences);
                    context.SaveChanges();
                }
                
                if(!context.Employees.Any())
                {
                    context.Employees.AddRange(FakeDataFactory.Employees);
                    context.SaveChanges();
                }
                if(!context.Customers.Any())
                {
                    context.Customers.AddRange(FakeDataFactory.Customers);
                    context.SaveChanges();
                }
                if(!context.Roles.Any())
                {
                    context.Roles.AddRange(FakeDataFactory.Roles);
                    context.SaveChanges();
                }
            }
        }
    }
}