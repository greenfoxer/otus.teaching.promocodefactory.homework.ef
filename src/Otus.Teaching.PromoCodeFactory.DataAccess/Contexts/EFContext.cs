using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Contexts
{
    public class EFContext: DbContext
    {
        public DbSet<Employee> Employees {get;set;}
        public DbSet<Role> Roles {get;set;}
        public DbSet<Customer> Customers {get;set;}
        public DbSet<Preference> Preferences {get;set;}
        public DbSet<PromoCode> PromoCodes {get;set;}
        public DbSet<CustomerPreference> CustomerPreferences {get;set;}
        public EFContext(DbContextOptions<EFContext> dbOptions) : base(dbOptions)
        { }
        protected override void OnConfiguring(DbContextOptionsBuilder modelBuilder)
        { }
         protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CustomerOptions());
            modelBuilder.ApplyConfiguration(new EmployeeOptions());
            modelBuilder.ApplyConfiguration(new RoleOptions());
            modelBuilder.ApplyConfiguration(new PreeferenceOptions());
            modelBuilder.ApplyConfiguration(new PromoCodeOptions());
            modelBuilder.ApplyConfiguration(new CustomerPreferenceOptions());
        }
    }
}