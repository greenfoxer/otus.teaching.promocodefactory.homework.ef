using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Contexts
{
    public class PromosDesignTimeContext : IDesignTimeDbContextFactory<EFContext>
    {
        public EFContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<EFContext>();
            optionsBuilder.UseSqlite("Filename=../Otus.Teaching.PromoCodeFactory.WebHost/testDB.db");
            return new EFContext(optionsBuilder.Options);
        }
    }
}