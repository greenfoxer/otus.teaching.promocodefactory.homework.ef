using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Contexts
{
    public class MigrationInitializer : IDbInitializer
    {
        EFContext _context;
        public MigrationInitializer(EFContext context)
        {
            _context = context;
        }
        public void InitDb()
        {
            var pendingMigrations = _context.Database.GetPendingMigrations().Count();
            var allMigrations = _context.Database.GetMigrations().Count();
            if(pendingMigrations > 0)
                _context.Database.Migrate();
            // Если БД создается в первый раз, то произведем наполнение
            if(pendingMigrations == allMigrations) 
                _context.Seed();
        }
    }
}