using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Contexts
{
    public class DropAndRecreateInitializer : IDbInitializer
    {
        EFContext _context;
        public DropAndRecreateInitializer(EFContext context)
        {
            _context = context;
        }
        public void InitDb()
        {
            _context.Database.EnsureDeleted();
            _context.Database.EnsureCreated();
            _context.Seed();
        }
    }
}