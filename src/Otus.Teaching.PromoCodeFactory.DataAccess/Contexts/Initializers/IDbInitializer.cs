using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Contexts
{
    public interface IDbInitializer
    {
        void InitDb();
    }
}