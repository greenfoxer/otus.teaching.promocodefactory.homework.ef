using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Contexts;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected EFContext _context;
        protected DbSet<T> Data;
        public EfRepository(EFContext context)
        {
            _context = context;
            Data = _context.Set<T>();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await Data.ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await Data.FirstOrDefaultAsync( p => p.Id == id);
        }

        public async Task AddAsync(T item)
        {
            await Data.AddAsync(item);
            await _context.SaveChangesAsync();
        }
        public async Task UpdateAsync(T item)
        {
            _context.Update(item);
            await _context.SaveChangesAsync();
        }
        public async Task RemoveAsync(Guid item)
        {
            Data.Remove(await GetByIdAsync(item));
            await _context.SaveChangesAsync();
        }

        public async Task RemoveRangeAsync(IEnumerable<T> range)
        {
            Data.RemoveRange(range);
            await _context.SaveChangesAsync();
        }
    }
}