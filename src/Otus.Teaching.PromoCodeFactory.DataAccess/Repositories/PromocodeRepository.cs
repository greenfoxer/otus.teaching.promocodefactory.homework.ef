using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Contexts;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class PromocodeRepository : EfRepository<PromoCode>, IPromoCodeRepository
    {
        ICustomerRepository _customerRepo;
        IRepository<Employee> _employeeRepo;
        public PromocodeRepository(EFContext context, ICustomerRepository custRepo, IRepository<Employee> emplRepo) : base(context)
        {
            _customerRepo = custRepo;
            _employeeRepo = emplRepo;
        }
        public async Task AddPromocodeByPreferenceAsync(PromoCode promo)
        {
            var manager = await _employeeRepo.GetByIdAsync(promo.PartnerManagerId);
           
            var usersWithPreference = await _context.Customers.Include(s => s.Promocodes).Include(t => t.CustomerPreferences).ToListAsync();
            List<CustomerPreference> customerPreferenceTarget = new List<CustomerPreference>();
            foreach (var user in usersWithPreference)
            {
                customerPreferenceTarget.AddRange(user.CustomerPreferences.Where(t => t.PreferenceID == promo.PreferenceId));
            }
            foreach(var custpref in customerPreferenceTarget)
            {
                PromoCode currentPromo = new PromoCode() {
                    Id = Guid.NewGuid(),
                    Code = promo.Code,
                    ServiceInfo = promo.ServiceInfo,
                    BeginDate = promo.BeginDate,
                    EndDate = promo.EndDate,
                    PartnerName = promo.PartnerName,
                    PreferenceId = promo.PreferenceId,
                    PartnerManagerId = promo.PartnerManagerId,
                    CustomerId = custpref.CustomerID
                };
                await _context.AddAsync(currentPromo);
                /*//Пытался сделать так, но почему-то не работает(Database operation expected to affect 1 row(s) but actually affected 0 row(s). Data may have been modified or deleted since entities were loaded.). Буду благодарен, если вы полскажете, реализовать такое DDD - поведение
                custpref.Customer.AddNewPromoCode(promo);
                await _customerRepo.UpdateAsync(custpref.Customer);*/
            }
            await _context.SaveChangesAsync();
        }
    }
}