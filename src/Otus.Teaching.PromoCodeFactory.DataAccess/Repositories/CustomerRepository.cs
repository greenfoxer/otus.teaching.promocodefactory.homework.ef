using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Contexts;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerRepository : EfRepository<Customer>, ICustomerRepository
    {
        IRepository<PromoCode> _pomocodeRepo;
        public CustomerRepository(IRepository<PromoCode> prefrepo, EFContext context) : base(context)
        {
            _pomocodeRepo = prefrepo;
        }

        public async Task<Customer> GetCustomerFullDataAsync(Guid id)
        {
            return await Data.Include(c => c.Promocodes).Include(c => c.CustomerPreferences).ThenInclude(p => p.Preference).FirstOrDefaultAsync( t => t.Id == id);
        }

        public async Task UpdateCustomerAsync(Customer customer, IEnumerable<Guid> prefernces)
        {
            var entity = await Data.FirstOrDefaultAsync(t => t.Id == customer.Id);
            //ВОПРОС: Можно ли сделать обновление более оптимальным?
            var newprefs = _context.Preferences.Where(p => prefernces.Contains(p.Id)).Select( t => new CustomerPreference { CustomerID = entity.Id, Customer = entity, PreferenceID = t.Id, Preference = t }).ToList();
            var curretnprefs = _context.CustomerPreferences.Where(t => t.CustomerID == entity.Id);
            _context.CustomerPreferences.RemoveRange(curretnprefs);
            entity.FirstName = customer.FirstName;
            entity.LastName = customer.LastName;
            entity.Email = customer.Email;
            entity.CustomerPreferences = newprefs;
            await UpdateAsync(entity);
        }
    }
}